var map = L.map('map', {
  crs: L.CRS.EPSG2056,
  maxBounds: L.TileLayer.Swiss.latLngBounds
})
map.fitSwitzerland()

// Ajouter la carte topo couleurs. Nous utilisons le plugin TileLayer.Swiss.
var stCouleurs = L.tileLayer.swiss()
stCouleurs.addTo(map)

// Ajouter les photos aériennes. Nous indiquons l'identifiant de la couche WMTS.
var stImages = L.tileLayer.swiss({
  layer: 'ch.swisstopo.swissimage',
  maxNativeZoom: 28
})


var stGris = L.tileLayer.swiss({
  layer: 'ch.swisstopo.pixelkarte-grau',
  maxNativeZoom: 28
})

var baseLayers = {
  "Carte couleurs": stCouleurs,
  "Carte noir/blanc": stGris,
  "Photos aériennes": stImages,
}

// Nous pouvons définir des couches à superposer de la même manière en utilisant les couches WMTS
// de Swisstopo (voir https://wmts.geo.admin.ch/EPSG/3857/1.0.0/WMTSCapabilities.xml).
var overlays = {
  "Chemins pédestres": L.tileLayer.swiss({
    layer: 'ch.astra.wanderland',
    format: 'png',
    maxNativeZoom: 26,
    opacity: 0.7,
  }),
}
L.control.layers(baseLayers, overlays).addTo(map);


// Malgré le système de coordonnées qui est en CH1903+/LV95,
// Leaflet prend toujours les coordonnées en Lat/Lng (EPSG 4326).
// Si nous avons des coordonnées en CH1903+/LV95, il faut donc d'abord
// les projeter en Lat/Lng. Voici un exmple:
var berne_lv95 = L.point(2600000, 1200000)   // point avec coordonneés suisses
var berne_wgs84 = L.CRS.EPSG2056.unproject(berne_lv95)    // projeter le point
L.marker(berne_wgs84).addTo(map)  // ajouter le point comme marqueur

// ou le tout en une seule ligne de code:
L.marker(L.CRS.EPSG2056.unproject(L.point(2534078, 1153160))).addTo(map)
