# Leaflet avec système de coordonnées suisse

Voici un exemple qui illustre comment on peut utiliser Leaflet avec les données Swisstopo en coordonnées suisses (EPSG 2056) au lieu du Web Mercator (EPSG 3857).

Cet exemple utilise le plugin [Leaflet.TileLayer.Swiss](https://leaflet-tilelayer-swiss.karavia.ch/).
